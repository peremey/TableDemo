import { IItem } from "src/BL/types"
import { api } from "src/boot/axios"

export const search = async (filter: string, top: number, skip: number) => {
    let items: IItem[] = []
    try {
        const response = await api.get('/api/internal/search/globalsearch/Search', { params: {
            filter: filter,
            top: top,
            skip: skip
        } })
        items = response.data.Items
    }
    catch {
        const maxI = 14830667
        let fakeItemsObj = '{"Items": ['
        for(let i = 14830637; i < maxI; i++) {
            fakeItemsObj += '{"ContentId": '+ i + ',"LevelId": 500,"ModuleId": 691,"ApplicationName": "Глоссарий","Created": "2023-04-12T13:37:36.18","Modified": "2023-04-12T13:37:36.18","Title": "Система SGRC","Data": [{"FieldName": "ID","FieldValue": "'+ i + '","FieldType": "TrackingID"},{"FieldName": "Термин","FieldValue": "Система SGRC","FieldType": "Text"}]}'
            if(i < maxI-1)
                fakeItemsObj += ','
        }
        fakeItemsObj += ']}'
        items = JSON.parse(fakeItemsObj).Items
    }
    return items
}