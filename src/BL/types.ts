type ValueOf<T> = T[keyof T]

export const SearchDefaultEnum = {
    Top: 30, Skip: 5, Filter: ""
} as const
export type SearchDefaultEnum = ValueOf<typeof SearchDefaultEnum>

export interface IItem {
    ContentId : number
    ModuleId: number
    LevelId: number
    ApplicationName: string
    Created:  string
    Modified: string
    Title: string
    Data: IDataItem[]
}

export interface IRowItem extends Omit<IItem, 'Data'> {
    Data: string;
  }

export interface IDataItem {
    FieldName: string,
    FieldValue: string,
    FieldType: string

}