import { IItem, SearchDefaultEnum } from "src/BL/types";
import { computed, ref } from "vue";
import { load, save } from "./storage";
import { search } from "src/api/search";

const items = ref<IItem[]>([])

const initItems = async () => {
    items.value = await search(SearchDefaultEnum.Filter, SearchDefaultEnum.Top, SearchDefaultEnum.Skip)
}
/*
const loadItems = () => {
    return load("items")
}

const saveItems = () => {
    save("items", items.value)
}*/

export const Items = computed(async () => {
    if(items.value.length == 0)
        await initItems()
    return items.value
}) 